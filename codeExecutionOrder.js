let posts = [
  {title: 'Post one', body: 'This is post one body'},
  {title: 'Post two', body: 'This is post two body'},
];

/**
 * Using Async/Await/Fetch
 */

// async function fetchUsers() {
//   const res = await fetch('https://jsonplaceholder.typicode.com/users'); //Bemærk at fetch function retunere et Promiss og et nestede svar
//   const data = await res.json(); //for at løse nedestede svar fra fetch funktionen
//   console.log(data);
//
// }
//
// fetchUsers();





/**
 * Using Async/Await
 */

// init();
//
// async function init() {
//   await createPost({title: "Post three", body: "This is post three body"});
//   getPosts();
// }
//
// function getPosts() {
//   setTimeout(() => {
//     let output = '';
//     posts.forEach((post, index) => {
//       output += '<li>' + post.title + '</li>';
//     });
//     document.body.innerHTML = output;
//   }, 1000);
// }
//
//
// function createPost(post) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       const error = false; //Bare fordi at dette laver ikke fejl
//       posts.push(post);
//       if (!error) {
//         resolve();
//       } else {
//         reject('Error: Something went wrong');
//       }
//     }, 2000);
//   });
// }




/**
 * Using Promise.all
 */

// const promise1 = Promise.resolve('Hello World')
// const promise2 = 10;
// const promise3 = new Promise((resolve, reject) =>
// setTimeout(resolve, 2000, 'Goodbye'));
// const promise4 = fetch('https://jsonplaceholder.typicode.com/users').then(res => res.json());
//
// Promise.all([promise1, promise2, promise3, promise4]).then(values => console.log(values));
//



/**
 * Using Promiss
 */

// function getPosts() {
//   setTimeout(() => {
//     let output = '';
//     posts.forEach((post, index) => {
//       output += '<li>' + post.title + '</li>';
//     });
//     document.body.innerHTML = output;
//   }, 1000);
// }
//
//
// function createPost(post) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       const error = false; //Bare fordi at dette laver ikke fejl
//       posts.push(post);
//       if (!error) {
//         resolve();
//       } else {
//         reject('Error: Something went wrong');
//       }
//     }, 2000);
//   });
// }
//
// createPost({title: "Post three", body: "This is post three body"})
//   .then(getPosts) //Bemærk ingen ()
//   .catch(err => console.log(err)) //Prøv at sætte error = true. Det giver en "Uncaught promise" with error in browser
//



/**
 * Using callback
 */

// function getPosts () {
//   setTimeout(()=> {
//     let output = '';
//     posts.forEach((post, index) => {
//       output += '<li>' + post.title + '</li>'  ;
//     });
//     document.body.innerHTML = output;
//   }, 1000);
// }
//
// function createPost (post, callback) {
//   setTimeout(()=> {
//     posts.push(post);
//     callback();
//   }, 2000)
// }
//
// createPost({title: "Post three", body: "This is post three body"}, getPosts); //Bemærk ikke noget () når man passer en callback funktion




/**
 * Regular expression
 */

// function getPosts () {
//  setTimeout(()=> {
//    let output = '';
//    posts.forEach((post, index) => {
//      output += '<li>' + post.title + '</li>'  ;
//    });
//    document.body.innerHTML = output;
//  }, 1000);
// }
//
// function createPost (post) {
//   setTimeout(()=> {
//     posts.push(post);
//   }, 2000)
// }
//
// createPost({title: "Post tree", body: "This is post three body"});
//
// getPosts();
