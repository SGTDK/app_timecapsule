import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Api} from '..';
import {Storage} from '@ionic/storage';
import {Events} from "ionic-angular";

@Injectable()
export class User {

  private username: string;
  private email: string;
  private userId: string;
  private userAccessToken: string; // this is accessToken when calling API
  private dropboxAccessToken: string;
  private dropboxAccountId: string;


  constructor(
    public api: Api,
    public storage: Storage,
    public events: Events,
  ) {

    /**
     * Constructor activities goes here
     */

  }

  /**
   * Functions goes here
   */

  getUserImages() {
    let userId = this.userId;
    const data = {
      dateTaken: "2001-02-07",
      email: "soren@trst.test"
    };
    let seq = this.api.get(`api/Clients/${userId}/images` + this.addAccessTokenToAPICalls(), data).share();
    seq.subscribe((res: any) => {
      console.log('Something came back now:  ', res);
    }, err => {
      console.error('GET_USER_IMAGES_ERROR: ', err);
    });
    return seq;

  }


  async initUser() {
    const [is_login, userId, userAccessToken] = await Promise.all([
      this.storage.get('is_login'),
      this.storage.get('userId'),
      this.storage.get('userAccessToken')
    ]);

    if ((is_login && (typeof userId == "string") && (typeof userAccessToken == "string"))) {
      await Promise.all([
        this.setUserId(userId),
        this.setUserAccessToken(userAccessToken)]);
    }
    return this.getUserAPI()
  }


  patchUserAPI() {
    let userId = this.userId;
    const data = {
      "email": this.email,
      "username": this.username,
      "dropboxAccessToken": this.dropboxAccessToken,
      "dropboxAccountId": this.dropboxAccountId
    };
    let seq = this.api.patch(`api/Clients/${userId}` + this.addAccessTokenToAPICalls(), data).share();
    seq.subscribe((res: any) => {
      if (typeof res.id !== 'undefined') {
        this.username = res.username;
        this.userId = res.id;
        this.email = res.email;

        if (typeof res.dropboxAccountId !== 'undefined') {
          this.dropboxAccountId = res.dropboxAccountId;
        }

        if (typeof res.dropboxAccountId !== 'undefined') {
          this.dropboxAccessToken = res.dropboxAccessToken;
        }
      } else {
        console.log('user.ts) Noget gik galt ved at behandle data fra patchUserAPI ');
      }
    }, err => {
      console.error('PATCHING_USER_DATA_ERROR', err);
    });
    return seq;
  }

// check https://forum.ionicframework.com/t/get-subscribe-data-out-of-function/76612/3
  getUserAPI() {
    let id = this.userId;
    let seq = this.api.get(`api/Clients/${id}` + this.addAccessTokenToAPICalls()).share();
    seq.subscribe((res: any) => {
      if (typeof res.id !== 'undefined') {
        this.username = res.username;
        this.userId = res.id;
        this.email = res.email;

        if (res.dropboxAccountId == '' || res.dropboxAccessToken == '') {
          console.log('--> Dropbox data does NOT exists, checked in user.ts');
          this.events.publish('dropbox:ready', false);
        } else {
          console.log('--> Dropbox data exists, checked in user.ts');
          console.log('DropboxId:', res.dropboxAccountId, '  DropboxAccesstokenres:', res.dropboxAccessToken);
          this.dropboxAccountId = res.dropboxAccountId;
          this.dropboxAccessToken = res.dropboxAccessToken;
          this.events.publish('dropbox:ready', true);
        }


        this.events.publish('user:exist', true);
      }
    }, err => {
      console.error('getUserAPI_ERROR', err);
      this.events.publish('user:exist', false);
    });
    return seq;
  }


  addAccessTokenToAPICalls() {
    return ('?access_token=' + this.userAccessToken);
  }

  setUserAccessToken(userAccessToken) {
    this.userAccessToken = userAccessToken;
  }

  getUserAccessToken() {
    return this.userAccessToken;
  }


  forgotPassword() {
    console.log('Something forgot passord procedure, to be implemented ');
  }

  changePassword() {
    console.log('Something to change the password ');
  }

  getUser() {
    return this;
  }

  getDropbocAccessToken() {
    return this.dropboxAccessToken;
  }

  setDropboxAcccessToken(dropboxAccessToken = '') {
    this.dropboxAccessToken = dropboxAccessToken;
    return this.patchUserAPI()
  }

  getDropboxAccountId() {
    return this.dropboxAccountId;
  }

  setDropboxAccountId(dropboxAccountId = '') {
    this.dropboxAccountId = dropboxAccountId;
    return this.patchUserAPI();
  }

  deleteUserAccount() {
    console.log('WARNING: The account will be deleted ');
  }


  getUserName() {
    return this.username;
  }

  setUserName(userName) {
    this.username = userName;
    this.patchUserAPI();
  }

  getUserId() {
    return this.userId;
  }

  setUserId(userId) { //no patching, cant change userId
    this.userId = userId;
  }

  getEmail() {
    return this.email;
  }

  setEmail(email) {
    this.email = email;
    this.patchUserAPI();
  }


  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post('api/Clients/login?include=user', accountInfo).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (typeof res.id !== 'undefined') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('LOGIN_ERROR', err);
    });
    return seq;
  }


  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    console.log('  TRYING TO SIGN UP');
    let seq = this.api.post('api/Clients', accountInfo).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (typeof res.id !== 'undefined') {
        //this._loggedIn(res);
        this.login(accountInfo);
      } else {
        console.log(' --- FAILED TO SIGN UP NEW USER SOMEHOW')
      }
    }, err => {
      console.error('ERROR in signup', err);
    });
    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  async logout() {
    //this.storage.clear().then(null);

    await this.storage.set('userId', null).then(() => {
      console.log('User.logout: UserId set to null');
    });
    await this.storage.set('is_login', null).then(function () {
      console.log('User.logout: is_login set to null');
    });
    await this.storage.set('userAccessToken', null).then(() => {
      console.log('User.logout: userAccessTokenset to null');
    });

    console.log(' user.logout triggered ');
  }

  /**
   * Process a login/signup response to store user data
   */
  async _loggedIn(resp) {

    await this.storage.set('userId', resp.userId).then(() => {
      //console.log('Logging in; userId set to ', resp.userId);
      this.userId = resp.userId;
    });
    await this.storage.set('is_login', true).then(function () {
      //console.log('Logging in; is_login is set to True');
    });
    await this.storage.set('userAccessToken', resp.id).then(() => {
      this.userAccessToken = resp.id;
      //console.log('Logging in; id/accessToken set to ', resp.id);
    });

    this.initUser();
  }


  updateImages(accountInfo: any) {
    let seq = this.api.post('api/Images/updateImages', accountInfo).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, the images are updated
      if (res.status == 'success') {
        console.log('--- Response from API: Images are updated  ---')
      } else {
      }
    }, err => {
      console.error('ERROR in updateImages', err);
    });

    return seq;
  }


}

