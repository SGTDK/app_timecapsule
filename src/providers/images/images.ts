import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Api} from '..';
import * as moment from "moment";
import {DropboxProvider} from "..";
import {Events} from "ionic-angular";
import {User} from "..";


@Injectable()
export class ImagesProvider {
  private imageArray: any;
  private numberOfImagesInAPI: any;

  constructor(
    public http: HttpClient,
    public api: Api,
    public dropboxProvider: DropboxProvider,
    public user: User,
    public events: Events,
  ) {

    /**
     * Constructor
     */

    this.events.subscribe('dropbox:ready', (status) => {
      if (status) {
        this.setImageArray()
        this.getNumberOfImagesFromAPI();
      }
    });


  }

  /**
   *  Normal functions here
   */

  getNumberOfImages () {
    return this.numberOfImagesInAPI;
  }


  getNumberOfImagesFromAPI () {
    let id = this.user.getUserId();
    let seq = this.api.get(`api/Clients/${id}/images/count` + this.user.addAccessTokenToAPICalls()).share();
    seq.subscribe((resp: any) => {
      console.log('There are now NumberOfImagesInAPI:  ', resp.count);
      this.numberOfImagesInAPI = resp.count;
      //return resp;
    }, err => {
      console.error('ERROR_getNumberOfImagesInAPI: ', err);
    });
    //return seq;
  }



  getImageArrayAPI(imageDate) {
    let src = this.getImagesFromDate(moment(imageDate).format("YYYY-MM-DD"));
    src.subscribe((resp) => {
      this.imageArray = resp;
      if (this.imageArray.length == 0) {
        console.log('image.ts: imageAray.length = 0 ');
        this.events.publish('imageArray:empty', true)
      } else {
        this.getTempImageLinkInToImageArray();
        this.events.publish('imageArray:empty', false)
      }
    });

    return src;
  }



  getImagesFromDate(imagedate) {
    let id = this.user.getUserId();
    const monthTaken = moment().format("MM");
    const dayTaken = moment().format("DD");
    const filter = "filter[where][and][0][monthTaken]="+monthTaken+"&filter[where][and][1][dayTaken]="+ dayTaken;
    let seq = this.api.get(`api/Clients/${id}/images` + this.user.addAccessTokenToAPICalls() + `&${filter}`).share();
    seq.subscribe((resp: any) => {
    }, err => {
      console.error('ERROR_getImagesFromDate: ', err);
    });
    return seq;
  }





  deleteImageListInAPI() {
    let id = this.user.getUserId();
    let seq = this.api.delete(`api/Clients/${id}/Images` + this.user.addAccessTokenToAPICalls()).share();
    seq.subscribe((resp: any) => {
      console.log(' Images are deleted. there are now (resp): ', resp);
    }, err => {
      console.error('ERROR in deleteImageListInAPI', err);
    });
    return seq;
  }


  updateImagelistInAPI() {
    let id = this.user.getUserId();
    let seq = this.api.get(`api/Clients/${id}/updateImages` + this.user.addAccessTokenToAPICalls()).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, the images are updated
      if (res.Status == 200) {
        console.log('--- Response from API: Images are updated  ---')
      } else {
        console.log(' Response from updateImagelistInAPI er mystisk tom: ', res);
      }
    }, err => {
      console.error('ERROR in updateImages', err);
    });
    return seq;
  }


  setImageArray(imageDate = null) {
    if (!imageDate) {
      imageDate = moment().format("YYYY-MM-DD")
    }
    this.getImageArrayAPI(imageDate);
  }


  getImageArray() {
    return this.imageArray;
  }


  getTempImageLinkInToImageArray() {
    let a = this.imageArray;
    for (let x = 0; x < a.length; x++) {
      this.dropboxProvider.getTempImageLinkFromDropbox(a[x]).then((dbx) => {
        if (x == a.length - 1) {
          this.events.publish('TempImageLink:Ready')
        }
      });
    }
  }


}


// setYearArrays() {
//   let a = this.imageArray;
//   let t = [];
//   for (let x = 0; x < a.length; x++) {
//     if (t.indexOf(a[x].yearTaken) == -1) {
//
//       // only happends first time a year shows up
//       t.push(a[x].yearTaken);
//       let year = JSON.stringify(a[x].yearTaken);
//       //this.yearArrays.push({Year : a[x].yearTaken, Images: [a[x]]});
//     }
//     // do something with every image
//     this.yearArrays.push({
//       Year : a[x].yearTaken,
//       Images: [a[x]],
//     });
//
//   }
// }
//

//
// sortYearArrays() {
//   //some sorting of the arrays:
//   for (const year in this.yearArrays) {
//     //let value = this.yearArrays[year]; Skal måske være på for at virke
//
//     //console.log(' String for; ', year, '    contains: ', JSON.stringify(value, null, 2));
//
//     if (this.yearArrays.hasOwnProperty(year)) {
//       //egenyear Array - gør noget
//       //console.log(' This YEAR ', year);
//       this.yearArrays[year].sort((a, b) => Number(a.unixTaken) - Number(b.unixTaken));
//
//     } else {
//       //property fra prototype Object - gør ingen ting
//
//     }
//   }
// }
//
// getYearArrays() {
//   return this.yearArrays;
// }
