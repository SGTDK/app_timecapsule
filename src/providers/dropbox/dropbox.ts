import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Api} from '..';
import Dropbox from "dropbox";
import {Events} from "ionic-angular";
import {User} from "..";
import {environment as ENV} from "../../environments/environment";



// læs om dropbox sdk her: http://dropbox.github.io/dropbox-sdk-js/Dropbox.html#filesDownload__anchor
// læs om dropbox endpoint her: https://www.dropbox.com/developers/documentation/http/documentation#files-get_temporary_link

@Injectable()
export class DropboxProvider {
  private appKey: any;
  private redirectURI: any;


  constructor(
    public user: User,
    public api: Api,
    public events: Events,
  ) {

    this.appKey = ENV.dropboxAppKey;
    this.redirectURI = ENV.dropboxRedirectURL;

    console.log('Test from dropbox.ts');

    this.events.subscribe('dropbox:ready', (status) => {
      if (!status) {
        console.log('User is created, but dropbox connection missing. Initiating dropbox flow ');
        this.initiateDropbox();
      }
    });
  }


  async initiateDropbox() {
    let dropboxReturnStatus = false;
    dropboxReturnStatus = await this.returnFromDropboxCheck();
    if (dropboxReturnStatus) {
      console.log('initiateDropbox: New Dropbox connection established from return data ');
      this.events.publish('dropbox:updateImagesInAPI', true); //Todo Gør det for hutigt. Skal await
    } else {
      console.log('initiateDropbox: No dropbox data in User or URL - New connection must be established');
      this.events.publish('dropbox:getNewConnection', true);
    }
  }


  async returnFromDropboxCheck() { //Todo Can den laves til "serie await" og alle de underlæggende function return - promise.all?
    let accessToken = await this.getAccessTokenFromUrl();
    let accountId = await this.getAccountIdFromUrl();
    let dropboxReturnStatus = await this.checkDropboxState(this.getStateFromUrl());
    if (typeof accessToken == 'string' && typeof accountId == 'string' && dropboxReturnStatus) {
      await this.user.setDropboxAcccessToken(accessToken);
      await this.user.setDropboxAccountId(accountId);
    }
    return dropboxReturnStatus;
  }


  async checkDropboxState(dropboxState) {
    return (this.user.getUserId() == dropboxState);
  }


  async getTempImageLinkFromDropbox(image) {
    let dbx = new Dropbox.Dropbox({accessToken: this.user.getDropbocAccessToken()});
    await dbx.filesGetTemporaryLink({
      path: image.imageId
    }).then((resp) => {
      image.link = resp['link'];
    }).catch((err) => {
      console.log('ERROR_getTempImageLinkFromDropbox: ', err);
    });
    return dbx;
  }


  signUpWithDropboxSdk() {
    let dbx = new Dropbox.DropboxTeam({clientId: this.appKey});
    console.log(' signUpWithDropboxSdk: redirectURI ');
    let authUrl = dbx.getAuthenticationUrl(this.redirectURI + '&state=' + this.user.getUserId());
    console.log('signUpWithDropboxSdk) authUrl: ', authUrl);
    window.location.href = authUrl;
  }

  getStateFromUrl() {
    return this.parseQueryString(window.location.hash).state;
  }

  getAccessTokenFromUrl() {
    return this.parseQueryString(window.location.hash).access_token;
  }

  getAccountIdFromUrl() {
    return this.parseQueryString(window.location.hash).account_id;
  }

  parseQueryString(str) {
    var ret = Object.create(null);

    if (typeof str !== 'string') {
      return ret;
    }

    str = str.trim().replace(/^(\?|#|&)/, '');

    if (!str) {
      return ret;
    }

    str.split('&').forEach(function (param) {
      var parts = param.replace(/\+/g, ' ').split('=');
      // Firefox (pre 40) decodes `%3D` to `=`
      // https://github.com/sindresorhus/query-string/pull/37
      var key = parts.shift();
      var val = parts.length > 0 ? parts.join('=') : undefined;

      key = decodeURIComponent(key);

      // missing `=` should be `null`:
      // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
      val = val === undefined ? null : decodeURIComponent(val);

      if (ret[key] === undefined) {
        ret[key] = val;
      } else if (Array.isArray(ret[key])) {
        ret[key].push(val);
      } else {
        ret[key] = [ret[key], val];
      }
    });

    return ret;
  }

}

