import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimecapsuleGalleryPage } from './timecapsule-gallery';
import {IonicImageLoader} from "ionic-image-loader";
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    TimecapsuleGalleryPage,
  ],
  imports: [
    IonicPageModule.forChild(TimecapsuleGalleryPage),
    IonicImageLoader,
    PipesModule,
  ],
})
export class TimecapsuleGalleryPageModule {}
