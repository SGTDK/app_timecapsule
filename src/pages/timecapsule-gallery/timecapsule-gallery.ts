import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ImagesProvider, User} from "../../providers";
import {MyApp} from "../../app/app.component";
import * as moment from "moment";


@IonicPage()
@Component({
  selector: 'page-timecapsule-gallery',
  templateUrl: 'timecapsule-gallery.html',
})
export class TimecapsuleGalleryPage {


  // public imageDate: any;
  public imageArray: any;
  public today: any;
  public imageArrayEmpty: boolean;


  constructor(
    public navCtrl: NavController,
    public user: User,
    public imagesProvider: ImagesProvider,
    public myApp: MyApp,
    public events: Events,
    public navParams: NavParams) {

    /**
     * Constructor
     */

    this.imageArray = myApp.imageArray;


    this.events.subscribe('imageArray:empty', (status) => {
      this.imageArrayEmpty = status;
    });

    console.log(' TimecapsuleGalleryPage constructor');
    // console.log(' TimecapsuleGalleryPage: this.user.getDropboxAccountId', JSON.stringify(this.user.getDropboxAccountId()));


    this.events.subscribe('TempImageLink:Ready', () => {
      this.imageArray = this.imagesProvider.getImageArray();
      //console.log('This imageArray: ', this.imageArray);

    });

    this.today = moment().format("Do of MMMM, YYYY");

  }


  /**
   * Functions
   */


  ionViewWillEnter() {
  }


  ionViewDidLoad() {
    console.log(' ionViewdidLoad. ImageArray ', this.imageArray);
    }

  ionViewCanEnter() {
    return typeof this.user.getDropboxAccountId() === 'string';
  }


}

