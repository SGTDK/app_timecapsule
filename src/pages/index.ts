
//export const FirstRunPage = 'TutorialPage'; //Todo Re-add this page, once configured to TCG - and know how to avoid it spamming each time I start the app

// The initial root pages for our tabs (remove if not using tabs)
export const Tab3Root = 'TimecapsuleGalleryPage';
export const Tab2Root = 'MyAccountPage';
export const Tab1Root = 'SlideshowPage';

// Other usefull pages
export const LoginPageLink = 'LoginPage';
export const SignupPage = 'SignupPage';
export const MainPage = 'TabsPage';


