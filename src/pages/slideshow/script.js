let session = null;

let imageURL = "https://dl.dropboxusercontent.com/apitl/1/AMeGkRRBEGPAUr8TuxaUdCEYyet6h9wWr2FSpLE_7ZtGLOD3GU0YGrmTpzN1W9w8AetNao1WRFSBUPXIcmksGUWq9wwdbA1cm3lQvwuKhT95K2QRXlnzwuNAx08Y1IYAaaft7lIPBBZ1Sa-uQSrRhxdbaxTs_2Jn6lBbKPfDRMgbzdKYWulmCrUNiuwxxwbKPh0a9TaQKAaTwXWFQ-xtjVsx4lDJ75dqaeCphmlJwuM4qvWg2snuYtImnk-2sK4Bzo3cHWw2vuyZ-rLZ2acylN_FKJyQm-D0xEC7FRp7pZc3GiZTPdDLAXmpdsssrylsGBUDooNo7zgwhzdhz3HzUoH2x_YijRZRt00AnOFdrEvdgA";

function someLog() {
  console.log("I was clickd");

}

function initializeCastApi() {
  let applicationID = chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID;
  let sessionRequest = new chrome.cast.SessionRequest(applicationID);
  let apiConfig = new chrome.cast.ApiConfig(sessionRequest,
    sessionListener,
    receiverListener);
  chrome.cast.initialize(apiConfig, onInitSuccess, onInitError);
}

function sessionListener(e) {
  session = e;
  console.log('New session');
  if (session.media.length != 0) {
    console.log('Found ' + session.media.length + ' sessions.');
  }
}

function receiverListener(e) {
  if( e === 'available' ) {
    console.log("Chromecast was found on the network.");
  }
  else {
    console.log("There are no Chromecasts available.");
  }
}

function onInitSuccess() {
  console.log("Initialization succeeded");
}

function onInitError() {
  console.log("Initialization failed");
}

function launchApp() {
  console.log("Launching the Chromecast App...");
  chrome.cast.requestSession(onRequestSessionSuccess, onLaunchError);
}

function onRequestSessionSuccess(e) {
  console.log("Successfully created session: " + e.sessionId);
  session = e;
  loadMedia();
}

function onLaunchError() {
  console.log("Error connecting to the Chromecast.");
}

function loadMedia() {
  if (!session) {
    console.log("No session.");
    return;
  }

  let mediaInfo = new
  chrome.cast.media.MediaInfo(imageURL);
  mediaInfo.contentType = 'image/jpg';

  let request = new chrome.cast.media.LoadRequest(mediaInfo);
  request.autoplay = true;

  session.loadMedia(request, onLoadSuccess, onLoadError);
}

function onLoadSuccess() {
  console.log('Successfully loaded image.');
}

function onLoadError() {
  console.log('Failed to load image.');
}

function stopApp() {
  session.stop(onStopAppSuccess, onStopAppError);
}

function onStopAppSuccess() {
  console.log('Successfully stopped app.');
}

function onStopAppError() {
  console.log('Error stopping app.');
}
