import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SlideshowPage } from './slideshow';
import {IonicImageLoader} from "ionic-image-loader";
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    SlideshowPage,
  ],
  imports: [
    IonicPageModule.forChild(SlideshowPage),
    IonicImageLoader,
    PipesModule,
  ],
})
export class SlideshowPageModule {}
