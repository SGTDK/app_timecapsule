import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ImagesProvider, User} from "../../providers";
import {MyApp} from "../../app/app.component";
import * as moment from "moment";



@IonicPage()
@Component({
  selector: 'page-slideshow',
  templateUrl: 'slideshow.html',
})
export class SlideshowPage {

  public imageArray: any;
  public today: any;
  public imageArrayEmpty: boolean;


  constructor(
    public navCtrl: NavController,
    public user: User,
    public imagesProvider: ImagesProvider,
    public myApp: MyApp,
    public events: Events,
    public navParams: NavParams) {

    /**
     * Constructor
     */

    console.log(' Slideshow.imagearray: ', this.imageArray);

    this.events.subscribe('imageArray:empty', (status) => {
      this.imageArrayEmpty = status;
    });


    this.events.subscribe('TempImageLink:Ready', () => {
      this.imageArray = this.imagesProvider.getImageArray();

    });

    this.today = moment().format("Do of MMMM, YYYY");

  }


  /**
   * Functions
   */


  ionViewWillEnter() {
  }


  ionViewDidLoad() {
    console.log('Slideshow.ts loaded  ');
  }

  ionViewCanEnter() {
    return typeof this.user.getDropboxAccountId() === 'string';
  }


}

