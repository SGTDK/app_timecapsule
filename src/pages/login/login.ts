import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { User } from '../../providers';
import {LoginPageLink, SignupPage } from '../';
import {MyApp} from "../../app/app.component";
import {Events} from "ionic-angular";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  account: { email: string, password: string } = {
    email: 'myEmail@example.com',
    password: 'myPassword'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(
    public navCtrl: NavController,
    public user: User,
    public myApp: MyApp,
    public toastCtrl: ToastController,
    public events: Events,
    public translateService: TranslateService)

  {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })

  }

  toSignup () {
    this.navCtrl.push(SignupPage)
  }

  forgotPassword () {
    this.user.forgotPassword();
  }



  // Attempt to login in through our User service
  async doLogin() {
    await this.user.login(this.account).subscribe((resp) => {
      //this.events.publish('app:reload', true);
    }, (err) => {
      this.navCtrl.push(LoginPageLink);
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
