import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {User, DropboxProvider, ImagesProvider} from "../../providers";
import {TranslateService} from '@ngx-translate/core';
import {App} from "ionic-angular";
import {Events} from "ionic-angular";
import {MyApp} from "../../app/app.component";



/**
 * Generated class for the MyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {
  newName: any;
  newEmail: any;
  numberOfImages:any;



  constructor(
    public navCtrl: NavController,
    public user: User,
    public dropboxProvider: DropboxProvider,
    public translateService: TranslateService,
    public imageProvider: ImagesProvider,
    public app: App,
    public myApp: MyApp,
    public events: Events,
    public navParams: NavParams)

  {
    /**
     * Insert constructor code here
     */

    this.numberOfImages = this.imageProvider.getNumberOfImages();



  }


  updateImagelistInAPI() {
    this.imageProvider.updateImagelistInAPI();
  }

  deleteImageListInAPI() {
    this.imageProvider.deleteImageListInAPI();
  }

  changePassword() {
    console.log(' Not implemented');
    // this.user.changePassword();
  }

  async logout() {
    await this.user.logout().then(()=> {
      this.events.publish('user:logout', true)
    });
  }

  setNewDropboxAccount() {

    this.navCtrl.push('DropboxPage');

  }

  setNewUserName() {
    this.user.setUserName(this.newName);
  }

  setNewEmail() {
    this.user.setEmail(this.newEmail);
  }

  async deleteDropboxAccount() {
    // Add some warning/confirmation and then delete old images på DB
    // use event publish
    this.user.setDropboxAccountId("");
    this.user.setDropboxAcccessToken("");
    await this.imageProvider.deleteImageListInAPI();
    await this.imageProvider.getNumberOfImagesFromAPI();
    this.navCtrl.push('DropboxPage');

  }

  deleteUserAccount() {  //
    //this.user.deleteUserAccount();
    console.log('Not implemented');
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad MyAccountPage');
  }

}
