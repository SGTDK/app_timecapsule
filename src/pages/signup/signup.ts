import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import {Storage} from '@ionic/storage';
//import { User, DropboxProvider } from '../../providers';
import { User } from '../../providers';

import { MainPage } from '../';
import {MyApp} from "../../app/app.component";


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  account: { username: string, email: string, password: string } = {
    username: 'myUserName',
    email: 'myEmail@example.com',
    password: 'myPassword'
  };

  // Our translated text strings
  private signupErrorString: string;

  constructor(
    public myApp: MyApp,
    public navCtrl: NavController,
    public user: User,
    //public dropboxProvider: DropboxProvider,
    public toastCtrl: ToastController,
    public storage: Storage,
    public translateService: TranslateService)

  {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }


  /**
   * Functions for Signup.ts starts here
   */



  doSignup() {
    // Attempt to login in through our User service
    this.user.signup(this.account).subscribe((resp) => {
      console.log('From doSignup in signup.ts. Resp = ', resp);
      this.navCtrl.push(MainPage);
    }, (err) => {

      this.navCtrl.push('LoginPage');

      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
