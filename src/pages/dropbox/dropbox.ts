import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DropboxProvider} from "../../providers";
import {MyApp} from "../../app/app.component";
import {User} from "../../providers";


/**
 * Generated class for the DropboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dropbox',
  templateUrl: 'dropbox.html',
})
export class DropboxPage {

  constructor(public navCtrl: NavController,
              public dropboxProvider: DropboxProvider,
              public myApp: MyApp,
              public user: User,
              public navParams: NavParams)
  {
    /**
     * Constructor
     */

    console.log(' Dropbox.ts constructor loaded  ');
  }

  /**
   * Functions
   */

  setNewDropboxAccount() {
    // Add some warning/confirmation and then delete old images på DB
    this.dropboxProvider.signUpWithDropboxSdk()
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad DropboxPage');
  }

  ionViewCanLeave () {
    if (typeof this.user.getDropboxAccountId() !== 'string') {
      console.log(' FALSE: DropboxPage: this.user.getDropboxAccountId', JSON.stringify(this.user.getDropboxAccountId()));
      return false;
    } else {
      console.log(' TRUE: DropboxPage: this.user.getDropboxAccountId', JSON.stringify(this.user.getDropboxAccountId()));
      return true;

    }

  }


}
