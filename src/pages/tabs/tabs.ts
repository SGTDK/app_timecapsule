import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Tab1Root, Tab2Root, Tab3Root } from '../';
import { User} from "../../providers";
import { MyApp} from "../../app/app.component";


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";

  constructor(
    public navCtrl: NavController,
    public translateService: TranslateService,
    public user: User,
    public myApp: MyApp,
    public storage: Storage,)

  {

    // Todo: Should be auto generated from page-title or something rather than using translate directly
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
    });
  }

  /**
   * Functions goes here
   */

  ionViewDidLoad() {
    // console.log('(tabs.ts) Current User: ', this.user.getUser());

  }

}
