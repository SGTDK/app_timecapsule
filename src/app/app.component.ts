import {Component, ViewChild, isDevMode} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {TranslateService} from '@ngx-translate/core';
import {Config, Nav, Platform} from 'ionic-angular';
import {DropboxProvider, ImagesProvider, User} from "../providers";
import {Storage} from '@ionic/storage';
import {ImageLoaderConfig} from "ionic-image-loader";
import {Events} from "ionic-angular";
import {environment as ENV} from "../environments/environment";

//import {FirstRunPage} from '../pages'; // Todo How and what is this?


@Component({
  template: `
    <ion-menu [content]="content">
      <ion-header>
        <ion-toolbar>
          <ion-title>Pages</ion-title>
        </ion-toolbar>
      </ion-header>

      <ion-content>
        <ion-list>
          <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
            {{p.title}}
          </button>
        </ion-list>
      </ion-content>

    </ion-menu>
    <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {

  rootPage = null;
  appGit: string = null;
  appDate: string = null;
  appDate1 = ENV.buildDate;
  appNavBarMessage: string = null;
  imageArray: any;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [ // Hvordan bruger man title herfra?
    {title: 'Dropbox', component: 'DropboxPage'},
    {title: 'Login', component: 'LoginPage'},
    {title: 'Menu', component: 'MenuPage'}, // Todo How and what is this?
    {title: 'Account', component: 'MyAccountPage'},
    {title: 'Slideshow', component: 'SlideshowPage'},
    {title: 'Settings', component: 'SettingsPage'},
    {title: 'Signup', component: 'SignupPage'},
    {title: 'Tabs', component: 'TabsPage'},
    {title: 'Timecapsule Gallery', component: 'TimecapsuleGalleryPage'},
    {title: 'Tutorial', component: 'TutorialPage'}, // Todo How and what is this?
    {title: 'Welcome', component: 'WelcomePage'}, // Todo How and what is this?
  ];

  constructor(
    private translate: TranslateService,
    private platform: Platform,
    private config: Config,
    private statusBar: StatusBar,
    public user: User,
    public events: Events,
    public storage: Storage,
    public imagesProvider: ImagesProvider,
    public dropboxProvider: DropboxProvider,
    private imageLoaderConfig: ImageLoaderConfig,
    private splashScreen: SplashScreen) {
    this.imageLoaderConfig.enableSpinner(true);
    this.initTranslate();
    this.statusBar.styleDefault();
    this.splashScreen.hide();
    this.appDate = this.appDate1;
    this.appGit = "177f9ba29bcbd22b6f2107be1ca2bf60d644b4dd";
    //this.appNavBarMessage = "Build: " + this.appDate + " Git: " + this.appGit;
    this.appNavBarMessage = "Build: " + this.appDate;


    platform.ready().then(() => {
      this.initiateApp().then((user$) => {
        console.log (' [platform.ready in app.compotents] Current User: ', this.user.getUser());
        //console.log('AppDate1', this.appDate1);
      });
    });


    // setting rootpage if user exist
    this.events.subscribe('user:exist', (status) => {
      if (status) {
        console.log('Event: Setting root page to TabsPage');
        this.rootPage = 'TabsPage';
      } else {
        console.log('Event: Setting root page to LoginPage');
        this.rootPage = 'LoginPage';
      }
    });


    // Just a way to reload app
    this.events.subscribe('app:reload', (status) => {
      if (status) {
        console.log('---->Event: Window.location.reload ');
        window.location.reload();
      }
    });


    //if user logout, do stuff
    this.events.subscribe('user:logout', (logout) => {
      if (logout) {
        console.log('Event: Logging user off, setting rootpage to LoginPage ');
        this.rootPage = 'LoginPage';
        this.events.publish('app:reload', true);
      }
    });


    //In order to set a new dropbox connection, function could be moved somewhere else
    this.events.subscribe('dropbox:getNewConnection', (status) => {
      if (status) {
        console.log('Event: Pushing page to DropboxPage to establish new connection ');
        this.nav.push('DropboxPage');
      }
    });

    //scanning dropbox and storeing at the API
    this.events.subscribe('dropbox:updateImagesInAPI', (status) => {
      if (status) {
        console.log('Event: Updating images in API from Dropbox, first scan ');
        this.imagesProvider.updateImagelistInAPI();
      }
    })


    //Setting app.imageArray
    this.events.subscribe('TempImageLink:Ready', () => {
      this.imageArray = this.imagesProvider.getImageArray();

    });



  }

  /**
   * Funktioner begynder her
   */



  async initiateApp() {
    return this.user.initUser();
  }


  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  ngOnInit() {
    if (isDevMode()) {
      console.log('👋 Development!');
    } else {
      console.log('💪 Production!');
    }
  }
}
