import {Pipe, PipeTransform} from '@angular/core';
import * as moment from "moment";

/**
 * Generated class for the YearsAgoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'yearsAgo',
})
export class YearsAgoPipe implements PipeTransform {
  /**
   * Takes a year in the past and compares it with the present year and returns the difference
   */
  transform(value: string, ...args) {
    let difYear = moment().subtract(value, 'years').format('YY') ;
    return difYear;
  }
}
