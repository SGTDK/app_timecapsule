import { NgModule } from '@angular/core';
import { GroupByPipe } from './group-by/group-by';
import { YearsAgoPipe } from './years-ago/years-ago';
@NgModule({
	declarations: [GroupByPipe,
    YearsAgoPipe],
	imports: [],
	exports: [GroupByPipe,
    YearsAgoPipe]
})
export class PipesModule {}
