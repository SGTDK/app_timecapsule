<h1> Must do before closed beta:</h1>

All todo items
Pauseskærm mens dropbox scanner - og så vis billeder
Ved ny sign-up skal der etableres dropbox OG scannes
Hvis der ikke er nogle billeder, kommer "ingen billeder i dag"-teksen ikke frem
Enable noget med password krav og uniqe email kontrol
Delete Account skal virker
Åben for at oprette nye brugere
Check gode råd fra web ang. launch sikkerhed på en ionic app/web-app
Lav mulighed for at "zooom" på bilelder, ved at scrolle på hjulet. 
Lav mulighed for "web-app" - genvej på desk-top (mobil)
Der mangler "back" eller "cancle" på dropboxconnection-siden...eller retter "back" virker ikke
Lav noget reload på "slideshow" (til chromecast)



<h1> Should do before open beta:</h1>
Setup Prod vs Dev environmental (hvad er "devDependencies fx)
Tilføj noget "API server online detect"
Opdater DOM/TCG-page ved forskellige begivenheder - kan det gøres uden reboot af app?
Dropbox ignore_folder
Der er noget galt hvis man logger ud og logger ind igen. Den reloader ikke siden
Side bliver ikke reloaded når der kommer nye infomration, fx. fra login status (check tcg.ts linje 36)
Error catching
Tilføj noget API-APP interface, fx. version verifikation
Sæt "spinning wheel" når der loades
Lav tekst pænere på "Dropbox" page
Fjern "blink" med nye dropbox information, samt ved login. Har brugt window.location.reload(), men bør loade DOM on-demand på en eller anden måde
identificere dropbox account på Account siden
Enable "Signup" igen


Can do:

Lav om sådan at der kommer et billede fra hver år og så en lightbox like feature
Lav styles ens på alle sider, ændre fra Ionic default
Lav en "slide" på billederne så man kan se mere information om billet
Add "number of images from year XXX" til overskriften på view
Option til Manifest.JSON: IOS/descktop: Browser vs Fullscreen
Billdet til ISO descktop er ikke en "Ikon", men en slags forside billede
Interface mellem API og APP, noget med opdatering, brug osv
Lav nogle "options": https://blog.ionicframework.com/customizing-ionic-apps-for-web-mobile/
Mulighed for at flytte billeder til dropbox_ignore (fx upassende eller ligegyldige)
Tilføj "dag" på billede tekst, fx "Monday, 2013.12.05 - 5 years ago"
Skriv noget data vdr. "Dropbox account" på "My accunt page" så man ved hvilke account er tilknytte
Lav en "tag til revision"/"hide" image feature






Sådan uploades ny version til server:
- Opdatere "../environments/environment";
- Brug FileZila til at delete WWW folder lokalt samt indholdet af "public_html" remote
- Copy paste til Command Prompt, i app-folderen: ionic cordova build browser --prod
- Kopiere alle filerne fra WWW (lokalt) til "public_html" remote
- Check det virker på en PC-browser samt en Mobil-browser
- Push branch til master-branch hos Bitbucket
